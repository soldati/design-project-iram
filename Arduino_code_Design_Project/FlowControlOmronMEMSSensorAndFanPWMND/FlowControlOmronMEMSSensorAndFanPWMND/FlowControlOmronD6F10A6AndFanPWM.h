/* CB 101 definition for the high voltage power supply 
 *  
 */

 #ifndef FlowControlOmronD6F10A6AndFanPWM_h
#define FlowControlOmronD6F10A6AndFanPWM_h

#include "Arduino.h"

// these extern variables shall be initialized in th sketch as this is a property of the arduino board 
extern  const double AnalogOutVoltMax; 
extern  const double AnalogOutNumber; 
extern  const double DigitalOutVoltMax; 
extern  const double DigitalOutNumber; 
extern  const double AnalogInVoltMax;
extern  const double AnalogInNumber;

extern const double CONST_fanControllerMinVoltage;
extern const double CONST_fanControllerMaxVoltage;
extern const double CONST_PWMCycleMinValue;
extern const double CONST_PWMCycleMaxValue;
extern const double CONST_FanPWMVoltInMax;

extern int recordIndex; 

extern bool recordArrayFullCheck;
//extern objects
//extern FlowControlOmronD6F10A6AndFanPWM NebulizerDilutionFlow;

//const static int CONST_lengthOfRecordingArray;
//double setPWMValuesRecord[]; 
//double observedFlowRateRecord[];

class FlowControlOmronD6F10A6AndFanPWM
{
  public:
  //these two  variables are property of the HV supply 
    
  //extern double correctionRatioRecord[];

    FlowControlOmronD6F10A6AndFanPWM(int,bool,int, int,bool,int,bool); //relaySensor, sharedSensor?, sensorPin, relayFan, sharedFan?,  FanPin,  JustMeterAndNoController?
    bool usesTheDigital5VPin2OrPin3();
    void powerUp();
    void powerDown(bool itIsUrgent);
    void setFlowTargetLPM(double flowSetPointLPM);
    void readFlowVoltage(); 
    double getFlowRateOfLastReadFlowVoltage();
    double convertFlowVoltageToFlowRateLPM(double flowVoltage);
    double convertFlowRateLPMToFlowVoltage(double flowRateLPM);
    void setFanSpeedFraction0to1(double fanSpeedFraction);
    void setDigitalFanSpeedFraction0to1(double fanSpeedFraction);
    void setAnalogFanSpeedFraction0to1(double fanSpeedFraction);
    double getFanSpeedFraction0to1();
    double getAverageFlowRateLPM();
    double computeAverageFlowVoltage();
    double computeFlowVoltageFractionalErrorUsingFlowVoltage();
    double computeFlowFractionalErrorUsingFlowRate();
    double averageOfCorrectionRatioRecord();
    double setDynamicPWMAndReturnAverageFlow(double cutoffErrorSet);
    double controlFlowForFanControlDuration(int FlowDelay, double cutoffErrorSet);
    
    bool get_isConnectionJustFlowMeterCheck();
  private:
    int _sensorRelaySwitchPin;
    bool _sensorRelaySwitchPinSharedCheck;
    int _fanRelaySwitchPin;
    bool _fanRelaySwitchPinSharedCheck;
    int _sensorVoltageOutputPin;
    int _fanPWMInputPin;
    double _flowSetPointLPM, _flowVoltageSetPoint;
    double _fanPWMFractionSetPoint; 
    double _flowVoltage,  _countFlowVoltage, _averageFlowVoltage; 
    double _cumulativeFlowVoltage;
    bool _isConnectionJustFlowMeterCheck;
    double _previousflowVoltageFracError;
    double _proportionalityConstant;
    double _lastReadFlowVoltage;
    double _convertFanSpeedFractionToDigitalPWMbit(double fanSpeedFraction);
    double _convertFanSpeedFractionToAnalogPWMbit(double fanSpeedFraction);
    
};

#endif
