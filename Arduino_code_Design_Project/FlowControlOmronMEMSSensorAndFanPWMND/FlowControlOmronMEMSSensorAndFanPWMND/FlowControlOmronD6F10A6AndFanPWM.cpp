/* CB 101 definition for the high voltage power supply 
 *  
 */

#include "Arduino.h"
#include "FlowControlOmronD6F10A6AndFanPWM.h"

const static int CONST_lengthOfRecordingArray=50;
 // double setPWMValuesRecord[CONST_lengthOfRecordingArray]; 
 // double observedFlowRateRecord[CONST_lengthOfRecordingArray];
  const double CONST_fanControllerMinVoltage = 0.6;//0.2;//changingforpump0.6; //0.6
  const double CONST_fanControllerMaxVoltage = 4.8;//4.8;//changingforpump4.4; //4.4
  const double CONST_PWMCycleMinValue = 0.1;//0.01;//changingforpump0.1;
  const double CONST_PWMCycleMaxValue = 1;
  const double CONST_FanPWMVoltInMax = 5.0;
  //double correctionRatioRecord[CONST_lengthOfRecordingArray];
  bool recordArrayFullCheck =false;
  int recordIndex=0; 
  
FlowControlOmronD6F10A6AndFanPWM::FlowControlOmronD6F10A6AndFanPWM(int sensorRelaySwitchPin, bool sensorRelaySwitchPinSharedCheck, int sensorVoltageOutputPin, int fanRelaySwitchPin, bool fanRelaySwitchPinSharedCheck, int fanPWMInputPin, bool isConnectionJustFlowMeterCheck)
{
  _sensorRelaySwitchPin= sensorRelaySwitchPin;
  _sensorRelaySwitchPinSharedCheck= sensorRelaySwitchPinSharedCheck;
  _fanRelaySwitchPin= fanRelaySwitchPin;
  _fanRelaySwitchPinSharedCheck= fanRelaySwitchPinSharedCheck;
  _sensorVoltageOutputPin= sensorVoltageOutputPin;
  _fanPWMInputPin= fanPWMInputPin;
  _isConnectionJustFlowMeterCheck = isConnectionJustFlowMeterCheck;
  _proportionalityConstant = 6;//4;//6;//0.1;//6;
  //these two  variables are property of the HV supply 
  
  
  
}

bool FlowControlOmronD6F10A6AndFanPWM::get_isConnectionJustFlowMeterCheck()
 {
  return _isConnectionJustFlowMeterCheck;
 }
bool FlowControlOmronD6F10A6AndFanPWM::usesTheDigital5VPin2OrPin3()
{
  if(_fanPWMInputPin ==2 || _fanPWMInputPin ==3)return true; 
  else return false; 
}
void FlowControlOmronD6F10A6AndFanPWM::powerUp()
{
 // Serial.println("relaySensor");
  pinMode(_sensorRelaySwitchPin,OUTPUT);
  pinMode(_fanRelaySwitchPin,OUTPUT);
  pinMode(_sensorVoltageOutputPin,INPUT);
  pinMode(_fanPWMInputPin,OUTPUT);
  if(_sensorRelaySwitchPinSharedCheck==1 || _sensorRelaySwitchPinSharedCheck==0 )
    digitalWrite(_sensorRelaySwitchPin,HIGH);
   // Serial.println(_sensorRelaySwitchPin);
  if(_fanRelaySwitchPinSharedCheck==1 || _fanRelaySwitchPinSharedCheck==0 )
  {  
    digitalWrite(_fanRelaySwitchPin,HIGH);
    //Serial.println(_fanRelaySwitchPin);
    analogWrite(_fanPWMInputPin,0);
  }
  //Serial.println(CONST_fanControllerMaxVoltage-CONST_fanControllerMinVoltage);
  
}

void FlowControlOmronD6F10A6AndFanPWM::powerDown(bool itIsUrgent)
{
  if(_sensorRelaySwitchPinSharedCheck==0 )
    digitalWrite(_sensorRelaySwitchPin,LOW);
  if(_fanRelaySwitchPinSharedCheck==0 )
  {  
    digitalWrite(_fanRelaySwitchPin,LOW);
  }
  if(itIsUrgent)
  {
    digitalWrite(_sensorRelaySwitchPin,LOW);
    digitalWrite(_fanRelaySwitchPin,LOW);
  }
}

void FlowControlOmronD6F10A6AndFanPWM::setFlowTargetLPM(double flowSetPointLPM)
{
  
  _flowSetPointLPM = flowSetPointLPM;
  _flowVoltageSetPoint = convertFlowRateLPMToFlowVoltage(flowSetPointLPM);
  _cumulativeFlowVoltage =0;
  _countFlowVoltage =0.0;
  _fanPWMFractionSetPoint = (_flowVoltageSetPoint-CONST_fanControllerMinVoltage)/(CONST_fanControllerMaxVoltage-CONST_fanControllerMinVoltage);
  //_fanPWMFractionSetPoint = 0.1;//+_fanPWMFractionSetPoint;
   //Serial.println(_fanPWMFractionSetPoint);
  setFanSpeedFraction0to1(_fanPWMFractionSetPoint);
}

void FlowControlOmronD6F10A6AndFanPWM::readFlowVoltage()
{
  double sensorValue =  analogRead(_sensorVoltageOutputPin);
  _flowVoltage=sensorValue/AnalogInNumber*AnalogInVoltMax;
 // Serial.println(sensorValue);
  _lastReadFlowVoltage = _flowVoltage;
  _cumulativeFlowVoltage = _cumulativeFlowVoltage + _flowVoltage;
  _countFlowVoltage = _countFlowVoltage+1.0;
}

double FlowControlOmronD6F10A6AndFanPWM::getFlowRateOfLastReadFlowVoltage()
{
  double sensorValue =  analogRead(_sensorVoltageOutputPin);
  _flowVoltage=sensorValue/AnalogInNumber*AnalogInVoltMax;
  return convertFlowVoltageToFlowRateLPM(_flowVoltage);
}
double FlowControlOmronD6F10A6AndFanPWM::computeAverageFlowVoltage()
{
  //Serial.println(_cumulativeFlowVoltage);
  //Serial.println(_countFlowVoltage);
  _averageFlowVoltage = _cumulativeFlowVoltage/_countFlowVoltage;
  //if(isnan(_averageFlowVoltage))_averageFlowVoltage = 1.1;
  _cumulativeFlowVoltage = 0;
  _countFlowVoltage =0.0; 
  //observedFlowRateRecord[recordIndex] = convertFlowVoltageToFlowRateLPM(_averageFlowVoltage);
  //if(recordIndex==0) correctionRatioRecord[recordIndex] =1;
  //else correctionRatioRecord[recordIndex] = convertFlowVoltageToFlowRateLPM(_averageFlowVoltage)/convertFlowVoltageToFlowRateLPM(_flowVoltageSetPoint);
  recordIndex++;
  if(recordIndex==CONST_lengthOfRecordingArray){recordIndex = 0; recordArrayFullCheck = true; }
  //Serial.println("_averageFlowVoltage");
  //Serial.println(_averageFlowVoltage);
  //Serial.println(convertFlowVoltageToFlowRateLPM(_averageFlowVoltage));
  return _averageFlowVoltage;
}

double FlowControlOmronD6F10A6AndFanPWM::computeFlowVoltageFractionalErrorUsingFlowVoltage()
{
  
  double fractionalError = (double) (_averageFlowVoltage-_flowVoltageSetPoint )/(CONST_fanControllerMaxVoltage -CONST_fanControllerMinVoltage); //convertFlowVoltageToFlowRateLPM(_flowVoltageSetPoint);
  //Serial.println();
  //Serial.println(fractionalError);
  //Serial.println(getFanSpeedFraction0to1());
  
  
  double historicCorrectionFactor = averageOfCorrectionRatioRecord();
  //Serial.println(fractionalError,4);
  
  //fractionalError=fractionalError;

  
  return fractionalError;
}
double FlowControlOmronD6F10A6AndFanPWM::computeFlowFractionalErrorUsingFlowRate()
{
  
  double fractionalError = (double) (-convertFlowVoltageToFlowRateLPM(_flowVoltageSetPoint) +convertFlowVoltageToFlowRateLPM(_averageFlowVoltage))/(convertFlowVoltageToFlowRateLPM(CONST_fanControllerMaxVoltage) -convertFlowVoltageToFlowRateLPM(CONST_fanControllerMinVoltage)); //convertFlowVoltageToFlowRateLPM(_flowVoltageSetPoint);
  return fractionalError;
}

double FlowControlOmronD6F10A6AndFanPWM::averageOfCorrectionRatioRecord()
{
  double sum =0.0;
  int len;
  if(recordArrayFullCheck) len = CONST_lengthOfRecordingArray;
  else len = recordIndex;
  
  for(int i=0; i<len;i++)
  {
    if(i==0) sum = sum+1.0;
    else{
    //    sum = sum+(observedFlowRateRecord[i]-observedFlowRateRecord[i-1])/(convertFlowVoltageToFlowRateLPM(_flowVoltageSetPoint)-observedFlowRateRecord[i-1]); 
    }
    
  }
  //sum = sum ;
  return sum/len;
}

double FlowControlOmronD6F10A6AndFanPWM::convertFlowVoltageToFlowRateLPM(double flowVoltage)
{
  double x = flowVoltage;
  double flowRateLPM = 0.00126135*pow(x,5) - 0.0301164*pow(x,4) + 0.281885*pow(x,3) - 1.18896*pow(x,2) + 4.6084*x - 3.67246;
  return flowRateLPM;

}

double FlowControlOmronD6F10A6AndFanPWM::convertFlowRateLPMToFlowVoltage(double flowRateLPM)
{
  double x = flowRateLPM;
  double flowVoltage = -1.42302*pow(10,-19)*pow(x,5) + 0.000130208*pow(x,4) - 0.00364583*pow(x,3) + 0.0307292*pow(x,2) + 0.327083*x + 1;
  return flowVoltage;
}

void FlowControlOmronD6F10A6AndFanPWM::setFanSpeedFraction0to1(double fanSpeedFraction)
{
  
  if(usesTheDigital5VPin2OrPin3()) setDigitalFanSpeedFraction0to1(fanSpeedFraction);
  else setAnalogFanSpeedFraction0to1(fanSpeedFraction);
  //setPWMValuesRecord[recordIndex] = _fanPWMFractionSetPoint;
  delay(200);
  
  
}
void FlowControlOmronD6F10A6AndFanPWM::setDigitalFanSpeedFraction0to1(double fanSpeedFraction)
{
  _fanPWMFractionSetPoint = fanSpeedFraction; 
  
  if(_fanPWMFractionSetPoint<CONST_PWMCycleMinValue) _fanPWMFractionSetPoint = CONST_PWMCycleMinValue; 
  if(_fanPWMFractionSetPoint>CONST_PWMCycleMaxValue) _fanPWMFractionSetPoint = CONST_PWMCycleMaxValue; 
  //Serial.println(_fanPWMFractionSetPoint);
  analogWrite(_fanPWMInputPin,_convertFanSpeedFractionToDigitalPWMbit(_fanPWMFractionSetPoint));
  
}

void FlowControlOmronD6F10A6AndFanPWM::setAnalogFanSpeedFraction0to1(double fanSpeedFraction)
{
  _fanPWMFractionSetPoint = fanSpeedFraction; 
  
  if(_fanPWMFractionSetPoint<CONST_PWMCycleMinValue) _fanPWMFractionSetPoint = CONST_PWMCycleMinValue;
  if(_fanPWMFractionSetPoint>CONST_PWMCycleMaxValue) _fanPWMFractionSetPoint = CONST_PWMCycleMaxValue;
  //Serial.println(_fanPWMFractionSetPoint);
  //Serial.println(_convertFanSpeedFractionToAnalogPWMbit(_fanPWMFractionSetPoint));
  analogWrite(_fanPWMInputPin,_convertFanSpeedFractionToAnalogPWMbit(_fanPWMFractionSetPoint));
}

double FlowControlOmronD6F10A6AndFanPWM::getFanSpeedFraction0to1()
{
  
  return _fanPWMFractionSetPoint;
}

double FlowControlOmronD6F10A6AndFanPWM::getAverageFlowRateLPM()
{
  return convertFlowVoltageToFlowRateLPM(_averageFlowVoltage);
}


double FlowControlOmronD6F10A6AndFanPWM::_convertFanSpeedFractionToDigitalPWMbit(double fanSpeedFraction)
{
  
  return fanSpeedFraction*CONST_FanPWMVoltInMax/DigitalOutVoltMax*DigitalOutNumber;
  //return (fanSpeedFraction*(CONST_fanControllerMaxVoltage-CONST_fanControllerMinVoltage)+CONST_fanControllerMinVoltage)/DigitalOutVoltMax*DigitalOutNumber;
}
double FlowControlOmronD6F10A6AndFanPWM::_convertFanSpeedFractionToAnalogPWMbit(double fanSpeedFraction)
{
  return fanSpeedFraction*CONST_FanPWMVoltInMax/AnalogOutVoltMax*AnalogOutNumber;
  //return (fanSpeedFraction*(CONST_fanControllerMaxVoltage-CONST_fanControllerMinVoltage)+CONST_fanControllerMinVoltage)/AnalogOutVoltMax*AnalogOutNumber;
}

double FlowControlOmronD6F10A6AndFanPWM::setDynamicPWMAndReturnAverageFlow(double cutoffErrorSet = 0.02)
{
  double cutoffError=cutoffErrorSet;
  //Serial.println("ControlFunction");
  double flowVoltageFracError = 1.0;
  computeAverageFlowVoltage(); //important as withougt this it wont work
  flowVoltageFracError = computeFlowVoltageFractionalErrorUsingFlowVoltage();
  double differenceInError = abs(flowVoltageFracError-_previousflowVoltageFracError);
  double proportionalityConstant = _proportionalityConstant;
  
  //Serial.print(_fanPWMInputPin);
  //Serial.print(", ");
  //Serial.println(flowVoltageFracError);
  
  //Serial.println(flowVoltageFracError);
  if(!_isConnectionJustFlowMeterCheck)
  {
    if(differenceInError>0.2)
    {
      cutoffError=0.2; 
      proportionalityConstant = _proportionalityConstant/10;
    } 
    else if (differenceInError>0.01)
    {
      cutoffError = cutoffErrorSet<0.1? 0.1 : cutoffErrorSet; 
      proportionalityConstant =_proportionalityConstant;
      //flowVoltageFracError = flowVoltageFracError*0.5; 
    }
    else if (differenceInError>0.001)//0.03//note that apart from the large values where the cutoff should be reduced it should not be done for small values.
    { 
      cutoffError = cutoffErrorSet<0.02 ? 0.02 : cutoffErrorSet;//cutoffErrorSet; 
      proportionalityConstant = _proportionalityConstant*10;
      //flowVoltageFracError = flowVoltageFracError*0.5; 
    }
    else if (differenceInError>0.0004)//0.03//note that apart from the large values where the cutoff should be reduced it should not be done for small values.
    {
      cutoffError = cutoffErrorSet;
      proportionalityConstant = _proportionalityConstant*30;
      //flowVoltageFracError = 0.005;// 0.005; 
    }
    else if (differenceInError>0.0001)//0.03//note that apart from the large values where the cutoff should be reduced it should not be done for small values.
    {
      cutoffError = cutoffErrorSet;
      proportionalityConstant = _proportionalityConstant*100;
      //flowVoltageFracError = 0.005;// 0.005; 
    }
    //Serial.println("flowVoltageFracError");
    //if(flowVoltageFracError<-1.0) flowVoltageFracError = -0.99;
    //Serial.println((-convertFlowVoltageToFlowRateLPM(_flowVoltageSetPoint) +convertFlowVoltageToFlowRateLPM(_averageFlowVoltage))/convertFlowVoltageToFlowRateLPM(_flowVoltageSetPoint)*1000);
    //Serial.println(differenceInError*1000);

    //setFanSpeedFraction0to1(getFanSpeedFraction0to1()*(1+flowVoltageFracError));
    if(flowVoltageFracError==_previousflowVoltageFracError) flowVoltageFracError = abs(_previousflowVoltageFracError)+0.1;
    //setFanSpeedFraction0to1(getFanSpeedFraction0to1()-(flowVoltageFracError)/abs(flowVoltageFracError)*_proportionalityConstant*getFanSpeedFraction0to1()*(flowVoltageFracError-_previousflowVoltageFracError));
    setFanSpeedFraction0to1(getFanSpeedFraction0to1()-(flowVoltageFracError)*proportionalityConstant*abs(flowVoltageFracError-_previousflowVoltageFracError));
    _previousflowVoltageFracError = flowVoltageFracError;
  }
  return getAverageFlowRateLPM();
}

double FlowControlOmronD6F10A6AndFanPWM::controlFlowForFanControlDuration(int FlowDelay = 3000, double cutoffErrorSet = 0.02)
{
  double cutoffError=cutoffErrorSet;
  //Serial.println("ControlFunction");
  double flowVoltageFracError = 1.0;
  computeAverageFlowVoltage();
      flowVoltageFracError = computeFlowVoltageFractionalErrorUsingFlowVoltage();
      //Serial.println(flowVoltageFracError);
      if(!_isConnectionJustFlowMeterCheck)
      {
        if(abs(flowVoltageFracError)>0.2)
        {
          cutoffError=0.2; 
        } 
        else if (abs(flowVoltageFracError)>0.1)
        {
          cutoffError = cutoffErrorSet<0.1? 0.1 : cutoffErrorSet; 
          flowVoltageFracError = flowVoltageFracError*0.5; 
        }
        else if (abs(flowVoltageFracError)>0.02)//0.03//note that apart from the large values where the cutoff should be reduced it should not be done for small values.
        { 
          cutoffError = cutoffErrorSet<0.02 ? 0.02 : cutoffErrorSet;//cutoffErrorSet; 
          flowVoltageFracError = flowVoltageFracError*0.5; 
        }
        else 
        {
          cutoffError = cutoffErrorSet;
          flowVoltageFracError = 0.005;// Changed 20230414 ND 0.005; 
        }
        
        //Serial.println("flowVoltageFracError");
        //Serial.println(flowVoltageFracError);

        //setFanSpeedFraction0to1(getFanSpeedFraction0to1()*(1+flowVoltageFracError));
        setFanSpeedFraction0to1(getFanSpeedFraction0to1()+_proportionalityConstant*(abs(flowVoltageFracError)-abs(_previousflowVoltageFracError)));
        _previousflowVoltageFracError = flowVoltageFracError;
      }
        //FlowController.setFanSpeedFraction0to1(FlowController.getFanSpeedFraction0to1()+FlowController.computeFlowVoltageFractionalError()/(1-0.27)*(0.95-0.1)); 
  /*unsigned long previousMillis = millis();
  //______20230328 Nikunj Removing the next line to handle floe delay in the file 
  //delay(FlowDelay);
  double cutoffError=cutoffErrorSet;
  //Serial.println("ControlFunction");
  double flowVoltageFracError = 1.0;
  int loopCount = 0;
  int averagingDelay = 1000; //milliseconds
  //while(millis()-previousMillisOuter<7*(1000+FlowDelay))
  while(abs(flowVoltageFracError)>cutoffError)
  {
    readFlowVoltage();
    if(millis()-previousMillis> averagingDelay) 
    {
      
      computeAverageFlowVoltage();
      flowVoltageFracError = computeFlowVoltageFractionalErrorUsingFlowVoltage();
      //Serial.println(flowVoltageFracError);
      if(!_isConnectionJustFlowMeterCheck)
      {
        if(abs(flowVoltageFracError)>0.2)
        {
          cutoffError=0.2; 
        } 
        else if (abs(flowVoltageFracError)>0.1)
        {
          cutoffError = cutoffErrorSet<0.1? 0.1 : cutoffErrorSet; 
          flowVoltageFracError = flowVoltageFracError*0.5; 
        }
        else if (abs(flowVoltageFracError)>0.02)//0.03//note that apart from the large values where the cutoff should be reduced it should not be done for small values.
        { 
          cutoffError = cutoffErrorSet<0.02 ? 0.02 : cutoffErrorSet;//cutoffErrorSet; 
          flowVoltageFracError = flowVoltageFracError*0.5; 
        }
        else 
        {
          cutoffError = cutoffErrorSet;
          flowVoltageFracError = 0.005; 
        }
        //Serial.println("flowVoltageFracError");
        //Serial.println(flowVoltageFracError);
        setFanSpeedFraction0to1(getFanSpeedFraction0to1()*(1+flowVoltageFracError));
        //FlowController.setFanSpeedFraction0to1(FlowController.getFanSpeedFraction0to1()+FlowController.computeFlowVoltageFractionalError()/(1-0.27)*(0.95-0.1)); 

        //______20230328 Nikunj Removing the next line to handle floe delay in the file 
        //delay(FlowDelay);//this delay is to prevent reading flow values while it is changing.. Very important one
        
        //20230328 CHANGE THIS SO THAT THE DELAY IS IN THE ARDUINO LOOP>> THAT WAY MULTIPLE CONTROLS CAN BE DONE SIMULTANEOUSLY
      }
      /*if(abs(flowVoltageFracError)<0.2) 
      {
        //flowVoltageFracError =flowVoltageFracError*0.5 ; //1.27 as seen from the historical correction factor.. 
        
        if(abs(flowVoltageFracError)<0.03) FlowController.setFanSpeedFraction0to1(FlowController.getFanSpeedFraction0to1()*(1+0.005));
        else FlowController.setFanSpeedFraction0to1(FlowController.getFanSpeedFraction0to1()*(1+flowVoltageFracError*0.5));
      }
      else FlowController.setFanSpeedFraction0to1(FlowController.getFanSpeedFraction0to1()*(1+flowVoltageFracError)); *

     
      previousMillis = millis();

      ////______20230328 Nikunj Removing the next three lines to handle floe delay in the file 
      //loopCount++;
      //if(_isConnectionJustFlowMeterCheck && loopCount>2)break;
      //else if(!_isConnectionJustFlowMeterCheck && loopCount>30*1000/averagingDelay) break;//if nomal control but takes longer than 30s then break..
      //______20230328 Nikunj Adding the next line to handle floe delay in the file  so it is just run once the loop.
      break;
    }
    
 
  }*/
  return getAverageFlowRateLPM();
}


