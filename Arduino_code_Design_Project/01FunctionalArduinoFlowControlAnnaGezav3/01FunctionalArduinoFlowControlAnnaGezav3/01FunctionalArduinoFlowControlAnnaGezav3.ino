#include <FlowControlOmronD6F10A6AndFanPWM.h>


#include <Wire.h>

//_______________Default constants for M-Duino ARA54+ - be cautious of needs of optoisolated inputs_______________
  const double AnalogOutVoltMax = 10.0; 
  const double AnalogOutNumber = 255.0; 
  const double DigitalOutVoltMax =5.0; 
  const double DigitalOutNumber =255.0; 
  const double AnalogInVoltMax = 10.0;
  const double AnalogInNumber = 1023.0;
  const int flowDelayMillis = 1500;//4000;//1500;//4000;//1500;
  const int averagingDelayMillis = 1000;//100;//not too low a value as all the loops cannot be checked
  //interesting - it worked very well for flowdelay1500 and averaging 500 and prop const 6- this is because they were then randomly controlled and this helped in control as 
 

 //NEW VARIABLES TO GO IN CONFIG FILE
 const int flowDelayMillisMinimum = 1000;
 const int flowDelayMillisMaximum = 5000;


//_______________Creating all Flow Control Objects_______________
FlowControlOmronD6F10A6AndFanPWM NebulizerDilutionFlow(30,true,54,28,true,3,true);//false //Pin3 // false means controller and true means just flow meter
FlowControlOmronD6F10A6AndFanPWM SheathFlow(30,true,55,28,true,5,false); // Pin 2
FlowControlOmronD6F10A6AndFanPWM CPCDilutionFlow(30,true,56,28,true,8,true); // pin 8 and 10 are garbage output
FlowControlOmronD6F10A6AndFanPWM ExitMakeupFlow(30,true,57,28,true,2,false); // pin 8 and 10 are garbage output


const int numberOfFlowControllers = 4;
String OrderedFlowControllersListNames[numberOfFlowControllers] = {"ExitMakeupFlow", "SheathFlow", "CPCDilutionFlow", "NebulizerDilutionFlow"};
FlowControlOmronD6F10A6AndFanPWM *OrderedFlowControllersList[numberOfFlowControllers] = {&ExitMakeupFlow, &SheathFlow, &CPCDilutionFlow, &NebulizerDilutionFlow};


//_______________Global variable decration_______________
unsigned long previousMillisForAllFlowControlCycle =0;
bool FlowSetCheck[numberOfFlowControllers] = {false,false,false,false};
double FractionalErrorInFlow[numberOfFlowControllers] = {0.2,0.2,0.2,0.2};

void ControlOrMeasureOneFlow(FlowControlOmronD6F10A6AndFanPWM FlowController, double absoluteDelay, int numberOfIterations, double cutoffErorrSetPoint=0.005)
{
  int i =0;
  while(i<numberOfIterations)
  {
    if(millis() - previousMillisForAllFlowControlCycle > absoluteDelay)
      
      FlowController.readFlowVoltage();
  
    if(millis() - previousMillisForAllFlowControlCycle > absoluteDelay+averagingDelayMillis)
    {
      
      FlowController.setDynamicPWMAndReturnAverageFlow(cutoffErorrSetPoint);//0.02
      previousMillisForAllFlowControlCycle = millis();
      i++;
    }
  }
  //if(millis() - previousMillisForAllFlowControlCycle > absoluteDelay+averagingDelayMillis) // as the last delay value plus the averaging delay should be the total loop time
    
    //Serial.println(previousMillisForAllFlowControlCycle);
}

void ControlOrMeasureAllFlows(FlowControlOmronD6F10A6AndFanPWM *FlowControllers[], double AbsoluteDelays[], double cutoffErorrSetPoint=0.005)
{
    //This code can be modified to change the flow control logic - usong absolute delays 
    // for eg. if absolute delays is all set to same value then the function should be such that it will read all flow meters for the averaging duration and then wait after the change with absolute delay
    // if it needs ot be one after then other, then say averaging delay of 0,flowDelay, 2*flowDelay.. so on.. this will have the read voltage casacaded and less prone to feedback from one other
    // could be also edited to have another array of avearging end array so that both are hard values./
    /*double limitingTime=AbsoluteDelays[numberOfFlowControllers-1];
    //double count=0.0;
    for(int k = 0; k<numberOfFlowControllers;k++)
    {
      if(!FlowControllers[k]->get_isConnectionJustFlowMeterCheck())
        if(limitingTime<FractionalErrorInFlow[k]*AbsoluteDelays[k]) limitingTime = FractionalErrorInFlow[k]*AbsoluteDelays[k];
      
    }*/
    bool resetTimeCheck = true;
    for (int j=0;j<numberOfFlowControllers;j++)
        if(FlowSetCheck[j]==false){ resetTimeCheck= false;break;}
   //if(millis() - previousMillisForAllFlowControlCycle > limitingTime) // as the last delay value plus the averaging delay should be the total loop time
   if(resetTimeCheck)
   { previousMillisForAllFlowControlCycle = millis();
      for (int j=0;j<numberOfFlowControllers;j++)
        FlowSetCheck[j] = false;
   //Serial.println("Reset");
   }
    //Serial.println(previousMillisForAllFlowControlCycle);
  //double cutoffErorrSetPoint=0.005;//0.5%
  
  for(int i = 0; i<numberOfFlowControllers;i++)
  {
   FlowControllers[i]->readFlowVoltage();
   FractionalErrorInFlow[i] = FlowControllers[i]->computeFlowFractionalErrorUsingFlowRate();
    if(AbsoluteDelays[i]*FractionalErrorInFlow[i] <flowDelayMillisMinimum) FractionalErrorInFlow[i] =flowDelayMillisMinimum/AbsoluteDelays[i];
    if(AbsoluteDelays[i]*FractionalErrorInFlow[i] >flowDelayMillisMaximum) FractionalErrorInFlow[i] =(double)flowDelayMillisMaximum/AbsoluteDelays[i];

   if(FractionalErrorInFlow[i]>1)FractionalErrorInFlow[i]=1;
    if(millis() - previousMillisForAllFlowControlCycle > AbsoluteDelays[i]*FractionalErrorInFlow[i]+averagingDelayMillis && FlowSetCheck[i]==false)
      {  FlowControllers[i]->setDynamicPWMAndReturnAverageFlow(cutoffErorrSetPoint);//0.02
        //Serial.println(i);}
        FlowSetCheck[i] = true;
      }
    else if(millis() - previousMillisForAllFlowControlCycle > AbsoluteDelays[i]*FractionalErrorInFlow[i])
    {  FlowControllers[i]->readFlowVoltage();
    }
  }
 
}

void printToCsv(char callingSource = 'l')
{
  if(callingSource == 's')//i.e. called from setup
  {
    //_______________First line of csv_______________
    Serial.print("Milliseconds");
    Serial.print(", "); 
    Serial.print("NebulizerDilutionFlow(LPM)");
    Serial.print(", ");
    Serial.print("SheathFlow(LPM)");
    Serial.print(", ");
    Serial.print("CPCDilutionFlow(LPM)");
    Serial.print(", ");
    Serial.print("ExitMakeupFlow(LPM)");
  }
  else // called from loop
  {
    //_______________Flow values back to the csv via serial_______________
    Serial.println();
    Serial.print(millis());
    Serial.print(", ");
    //Serial.print(NebulizerDilutionFlow.getAverageFlowRateLPM());
    Serial.print(NebulizerDilutionFlow.getFlowRateOfLastReadFlowVoltage());
    Serial.print(", ");
    Serial.print(SheathFlow.getFlowRateOfLastReadFlowVoltage());
    Serial.print(", ");
    Serial.print(CPCDilutionFlow.getFlowRateOfLastReadFlowVoltage());
    Serial.print(", ");
    Serial.print(ExitMakeupFlow.getFlowRateOfLastReadFlowVoltage());
  
  }
    
}

void setup() {
  Wire.begin();
  Serial.begin(9600UL);
  //_______________Power up the controllers and meters_______________
  NebulizerDilutionFlow.powerUp();
  SheathFlow.powerUp();
  CPCDilutionFlow.powerUp();
  ExitMakeupFlow.powerUp();
  //_______________Set flow on the controllers_______________
  NebulizerDilutionFlow.setFlowTargetLPM(1.6); //5LPM for2.5, 11LPM for PM1, 3.3 LPM for PM4, 1.5LPM for PM10 ; 1.75 LPM aerosol flow
  SheathFlow.setFlowTargetLPM(4.2); // 5.95 total flow with 4.2LPM sheath
  CPCDilutionFlow.setFlowTargetLPM(0.8);
  ExitMakeupFlow.setFlowTargetLPM(3.55);
  //_______________Allow first round round to 10% error_______________
  double AbsoluteDelays[numberOfFlowControllers] = {flowDelayMillis*2,flowDelayMillis*2,flowDelayMillis*2,flowDelayMillis*2};
  delay(5000);
  ControlOrMeasureAllFlows(OrderedFlowControllersList, AbsoluteDelays, 0.1);
  //ControlOrMeasureOneFlow(ExitMakeupFlow,flowDelayMillis,10,0.1);
  //ControlOrMeasureOneFlow(SheathFlow,flowDelayMillis,10,0.1);
  //delay(10000); 

   //print to csv
  printToCsv('s');
}


void loop() {

//_______________Finer control to 0.5% error_______________
  //double AbsoluteDelays[numberOfFlowControllers] = {flowDelayMillis,flowDelayMillis*2+averagingDelayMillis,flowDelayMillis*2+averagingDelayMillis*2,flowDelayMillis*2+averagingDelayMillis*3};
  double AbsoluteDelays[numberOfFlowControllers] = {flowDelayMillis,flowDelayMillis*2+averagingDelayMillis,flowDelayMillis*2+averagingDelayMillis*2,flowDelayMillis*2+averagingDelayMillis*3};
      ControlOrMeasureAllFlows(OrderedFlowControllersList, AbsoluteDelays);//use default control cut off
 //print to csv
 printToCsv();
}
