import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import os
from scipy.interpolate import interp1d
from scipy.signal import medfilt, detrend, find_peaks
from scipy.ndimage import gaussian_filter1d
import csv, json
plt.style.use('bmh')


def baseline_correct_and_cut(Absorbance, wavenumbers, Absorbance_collected, wavenumber_collected, cut_lower_limit=700,
                             outputfolder='',
                             exp_num=None, peak_locs=[1110, 1410], peak_height=0.001, distance_between_peaks=50,
                             baseline_range = [0,2100], fix_baseline_cuts = [900, 3600], baseline_points = [],
                             base_index_calculation = True,  baseline_deg = 4):
    """
    Perform baseline correction and cut of absorbance data.

    Parameters:
        Absorbance (ndarray): Absorbance data array to be used for baseline correction.
        wavenumbers (ndarray): Wavenumbers array corresponding to the absorbance data.
        Absorbance_collected (ndarray): Absorbance data array to be corrected.
        wavenumber_collected (ndarray): Wavenumbers array corresponding to the absorbance data to be corrected.
        cut_lower_limit (float): Lower limit for wavenumbers to be included in the output.

    Returns:
        y_corr (ndarray): Corrected absorbance data array.
        x (ndarray): Wavenumbers array corresponding to the corrected absorbance data.
        baselined_fig: Plot of the baseline corrected data.
    """
    # Trim data based on cut_lower_limit
    idx = np.where(wavenumbers > cut_lower_limit)[0]
    Absorbance = Absorbance[idx]
    wavenumbers = wavenumbers[idx]
    idx = np.where(wavenumber_collected > cut_lower_limit)[0]
    Absorbance_collected = Absorbance_collected[idx]
    wavenumber_collected = wavenumber_collected[idx]


    # Perform initial correction
    Yout = Absorbance_collected - Absorbance
    Yout = detrend(Yout, type='linear')
    Yout2 = Yout - np.min(Yout)

    # Trim data based on cut_lower_limit
    idx = np.where(wavenumbers > cut_lower_limit)[0]
    Absorbance = Absorbance[idx]
    wavenumbers = wavenumbers[idx]
    idx = np.where(wavenumber_collected > cut_lower_limit)[0]
    Absorbance_collected = Absorbance_collected[idx]
    wavenumber_collected = wavenumber_collected[idx]

    # Perform initial correction
    Yout = Absorbance_collected - Absorbance
    Yout = detrend(Yout, type='linear')
    Yout2 = Yout - np.min(Yout)

    # Spline and slope
    x = wavenumbers
    y_abs = Yout2
    samplingRateIncrease = 10
    newXSamplePoints = np.linspace(x[0], x[-1], len(x) * samplingRateIncrease)
    smoothedY = interp1d(x, y_abs, kind='cubic')(newXSamplePoints)
    slopes = np.append(0, np.diff(smoothedY))
    k_mean_abs = len(newXSamplePoints) // 4
    Mov_mean_abs = gaussian_filter1d(smoothedY, sigma=k_mean_abs)
    Mov_mean_abs = 1.03 * Mov_mean_abs
    k_mean = 210
    k_std = 100
    Mov_mean_slopes = gaussian_filter1d(slopes, sigma=k_mean)
    Mov_std_slopes = gaussian_filter1d(slopes, sigma=k_std)

    # Identify baseline points
    if base_index_calculation == True:
        y_mean = Mov_mean_slopes
        ystat_mean = np.array([[np.mean(y_mean), np.std(y_mean)],
                                [(np.mean(y_mean) - np.std(y_mean) / 5), (np.mean(y_mean) + np.std(y_mean) / 5)]])

        y_std = Mov_std_slopes
        ystat_std = np.array([[np.mean(y_std), np.std(y_std)],
                                [(np.mean(y_std) - np.std(y_std)), (np.mean(y_std) + np.std(y_std))]])

        ystat_abs = np.array([[np.mean(y_abs), np.std(y_abs)],
                                [(np.mean(y_abs) - np.std(y_abs)), (np.mean(y_abs) + np.std(y_abs))]])

        base_index = np.where((y_std <= np.log(2) * ystat_std[0, 0]) &
                                (y_mean > ystat_mean[1, 0]) &
                                (y_mean < ystat_mean[1, 1]) &
                                (smoothedY < Mov_mean_abs))[0]

        base_index = base_index[np.where((newXSamplePoints[base_index] >= baseline_range[0]) | (newXSamplePoints[base_index] <= baseline_range[1]))]
    else :
        base_index = np.array([])
    
    fix_index = np.where((newXSamplePoints >= fix_baseline_cuts[1]) | (newXSamplePoints <= fix_baseline_cuts[0]))[0]
    
    # ALL base indexes
    base_index = np.union1d(base_index, fix_index).astype(int)
    
    # Handpicked supplementary indexes
    list_of_values_in_X = [newXSamplePoints[np.argmin(np.abs(newXSamplePoints - supp_x_i))] for supp_x_i in baseline_points]
    supp_indexes = np.where(np.isin(newXSamplePoints, list_of_values_in_X))
    #print(supp_indexes)
    base_index = np.union1d(base_index, supp_indexes)

    # Spline and slope
    x = wavenumbers
    y_abs = Yout2
    samplingRateIncrease = 10
    newXSamplePoints = np.linspace(x[0], x[-1], len(x) * samplingRateIncrease)
    smoothedY = interp1d(x, y_abs, kind='cubic')(newXSamplePoints)
    Baseline_A = np.polyfit(newXSamplePoints[base_index], smoothedY[base_index], baseline_deg)

    y_corr = Yout2-np.polyval(Baseline_A, wavenumbers)

    # Find the peaks (around 1110 cm^-1 and 1410 cm^-1 for AmmSulfate)
    all_peaks, _ = find_peaks(y_corr, height=peak_height, distance=distance_between_peaks)
    # print(all_peaks)
    peaks_index = []
    for peak_loc in peak_locs:
        peak_i = np.argmin(np.abs(x[all_peaks] - peak_loc))
        peaks_index.append(all_peaks[peak_i])

    # Figure for baseline
    baselined_fig, axs = plt.subplots(3, 1, figsize=(9, 6))
    axs[0].plot(wavenumbers, Absorbance, label='clean crystal')
    axs[0].plot(wavenumber_collected, Absorbance_collected, label='loaded crystal')
    axs[0].vlines([x[peaks_index]], ymin=np.min(Absorbance_collected), ymax=np.max(Absorbance_collected),
                linestyles='--')
    axs[0].set_ylabel("Absorbance")
    axs[0].invert_xaxis()

    axs[1].plot(wavenumbers, Yout2, label='Difference', color='orange')
    axs[1].scatter(newXSamplePoints[base_index], smoothedY[base_index], label='baseline point')
    axs[1].plot(wavenumbers, np.polyval(Baseline_A, wavenumbers), label='baseline', color= 'b', alpha=0.4)
    axs[1].vlines([x[peaks_index]], ymin=0, ymax=np.max(Yout2), linestyles='--')
    axs[1].set_ylabel("Absorbance")
    axs[1].invert_xaxis()
    
    axs[2].plot(wavenumbers, y_corr, label='Corrected difference', color='black')
    axs[2].vlines([x[peaks_index]], ymin=0, ymax=1.2 * np.max(y_corr), linestyles='--', label="AmmSul peaks")
    axs[2].set_ylabel("Absorbance")
    axs[2].set_xlabel(r"Wavelength [$cm^{-1}$]")
    axs[2].invert_xaxis()
    
    baselined_fig.legend(bbox_to_anchor=(0.5, -0.07), loc='lower center', ncol=4)
    # save figure
    output_fig_path = os.path.join(outputfolder,f"FTIR_comparison_Exp{exp_num}_deg4.png")
    baselined_fig.savefig(output_fig_path, dpi=200, bbox_inches='tight')
    print(f"saved fig to ... {output_fig_path}")

    return x, y_corr, peaks_index

def All_Absorbance_Analysis(excel_file="Data/AmmSul/samples.xlsx", data_path="Data/AmmSul/FTIR_Data/",
                            output_folder="Data/AmmSul/OUTPUT/", peak_locations=[1110, 1410], peak_height=0.001,
                            distance_between_peaks=50,
                            baseline_range = [0,2100],
                            fix_baseline_cuts = [900, 3600], 
                            baseline_points = [],
                            base_index_calculation = True,
                            baseline_deg = 4):
    '''
    Does the FTIR analysis on all experiments reported in the excel file. Files and figures are saved in the OUTPUT folder.
    :param excel_file: Excel file with the reported experiments sample number, file names, etc..
    :return:
    '''
    # Open Excel file
    experiments_excel = pd.read_excel(excel_file)

    # Dictionary for saving values of experiments number, associated peak's wavelengths and absorbance
    peaks_dict = {'exp': [],
                  'wv': [],
                  'Abs': []}

    # Loop over each experiment
    for exp in experiments_excel["Sample"].unique():
        print(f"Processing experiment {exp}........")
        # Select the row with necessary information
        row = experiments_excel[experiments_excel["Sample"] == exp]
        row = row[row["aperture"] == 6.0]
        if row.shape[0] > 1:
            print(f"error, row with aperture 6 not unique for exp {exp}")

        # Open files for clean and loaded FTIR analysis
        loaded_ftir_file = os.path.join(data_path,row["FTIR_loaded"].values[-1])
        clean_ftir_file = os.path.join(data_path, row["FTIR_clean"].values[-1])
        col_dt = np.loadtxt(loaded_ftir_file, delimiter=",")  # , encoding="ANSI")

        if col_dt is None:
            print(f"Couldn't open file {loaded_ftir_file}")
            return False
        clean_dt = np.loadtxt(clean_ftir_file, delimiter=",")  # , encoding="ANSI")
        if clean_dt is None:
            print(f"Couldn't open file {clean_ftir_file}")
            return False

        # Extract arrays and inverse them to have increasing x axis
        Abs_clean = clean_dt[::-1, 1]
        Wv_clean = clean_dt[::-1, 0]

        Abs_col = col_dt[::-1, 1]
        Wv_col = col_dt[::-1, 0]

        # Cutoff the lower infrared wavelengths due to material's absorbance
        material = row["Material"].values[-1]
        if (material == 'ZnSe' or material == 'Air'):
            cut_lower_limit = 700
        elif (material == 'PS'):
            cut_lower_limit = 1000
        else:
            print(f"No lower cut for {material}")
            cut_lower_limit = 0

        # RUN the baseline correct and cut algorithm
        x, y_corr, peaks_index = baseline_correct_and_cut(Abs_clean, Wv_clean, Abs_col, Wv_col,
                                                          cut_lower_limit=cut_lower_limit,
                                                          outputfolder=output_folder,
                                                          exp_num=exp,
                                                          peak_locs=peak_locations,
                                                          peak_height=peak_height,
                                                          distance_between_peaks=distance_between_peaks,
                                                          baseline_range = baseline_range,
                                                          fix_baseline_cuts = fix_baseline_cuts,
                                                          baseline_points = baseline_points,
                                                          base_index_calculation = base_index_calculation,
                                                          baseline_deg = int(baseline_deg))

        results = {'Experiment Number': int(exp),
                   'Wavenumbers': x.tolist(),
                   'Corrected difference': y_corr.tolist(),
                   'wavenumber_peaks': x[peaks_index].tolist(),
                   'absorbance_peaks': y_corr[peaks_index].tolist()
                   }

        # Open a new CSV file for writing
        output_csv_filename = os.path.join(output_folder,f'Exp{exp}FTIRResults.csv')
        with open(output_csv_filename, 'w', newline='') as csvfile:
            # Define the field names for the CSV file
            fieldnames = list(results.keys())

            # Create a writer object for the CSV file and write the header row
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            # Write the data rows to the CSV file
            writer.writerow(results)

        # Same but json
        output_json_path = os.path.join(output_folder, f'Exp{exp}FTIRResults.json')
        with open(output_json_path, 'w') as fp:
            json.dump(results, fp)

        print(f"saved results to ... {output_csv_filename} and {output_json_path}")

        # Append to global analysis
        peaks_dict["exp"].append(int(exp))
        peaks_dict["wv"].append(x[peaks_index].tolist())
        peaks_dict["Abs"].append(y_corr[peaks_index].tolist())

    # Open a new CSV file for writing the peaks result
    with open(os.path.join(output_folder,'PeaksFTIRResults.csv'), 'w', newline='') as csvfile:
        # Define the field names for the CSV file
        fieldnames = list(peaks_dict.keys())

        # Create a writer object for the CSV file and write the header row
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        # Write the data rows to the CSV file
        writer.writerow(peaks_dict)

    # Same but json
    output_json_peakfile = os.path.join(output_folder,'PeaksFTIRResults.json')
    with open(output_json_peakfile, 'w') as fp:
        json.dump(peaks_dict, fp)

    #saving the peaks wavelengths as an array
    output_array_file_peakswavelengths = os.path.join(output_folder,"PeaksWavelengthsArray.csv")
    np.savetxt(output_array_file_peakswavelengths, np.array(peaks_dict["wv"]), delimiter=",", fmt='%10.5f')
    # Saving also the absorbance values related to the peaks as an array
    output_array_file_peakabsorbance =os.path.join(output_folder,"PeaksAbsorbanceArray.csv")
    np.savetxt(output_array_file_peakabsorbance, np.array(peaks_dict["Abs"]), delimiter=",", fmt='%10.5f')

    print(
        f"saved all results to ... {output_json_peakfile}, {output_array_file_peakswavelengths} and {output_array_file_peakabsorbance}")
    return True

def ShoowAllCorrectedAbsorbances(excel_datafrane, output_folder_path, nist_filepath= ''):
    plt.style.use('bmh')
    
    # creating a dictionary
    font = {'size': 12}
        # using rc function
    plt.rc('font', **font)
    
    fig, ax = plt.subplots(figsize=(12,2.5), dpi=400)
        
    n = int(1.5*len(excel_datafrane["Sample"].unique()))
    cmap = mpl.colormaps['twilight']
    colors = cmap(np.linspace(0,1,n))
    
    for exp in excel_datafrane["Sample"].unique():
        FTIR_result_path = os.path.join(output_folder_path, f"Exp{exp}FTIRResults.json")
        if not os.path.exists(FTIR_result_path):
            raise NotADirectoryError(f"FTIR results path is not a valid directory: {FTIR_result_path}")
        with open(FTIR_result_path) as json_file:
            FTIR_results_dict = json.load(json_file)
            
        ax.plot(FTIR_results_dict["Wavenumbers"], FTIR_results_dict["Corrected difference"],
                color = colors[exp], alpha=0.7, label=f"Exp {exp}")
    
    
    if os.path.exists(nist_filepath):  
        ax_other = ax.twinx()   
        nist_vals = pd.read_csv(nist_filepath, delimiter=";")
        ax_other.plot(nist_vals["wavenumber"], nist_vals["absorbance"], linestyle = 'dotted', linewidth=1.8, color = "black", alpha=0.5, label=f"NIST reference")
        ax_other.set_ylabel("NIST ref. absorbance", labelpad=10)
        ax_other.legend(loc='upper right', fontsize='9')
        ax_other.set_xlim([500,4200])
        ax_other.invert_xaxis()
    
    ax.invert_xaxis()
    ax.set_xlabel(r"wavenumber $[cm^{-1}]$")
    ax.set_ylabel("corrected absorbance")
    ax.legend(fontsize="9", loc='upper left')
    ax.set_xlim([500,4200])
    ax.invert_xaxis()
    plt.savefig(os.path.join(output_folder_path, "AllCorrectedAbsorbances.jpg"),dpi=400)
    plt.show()
    
    plt.rcdefaults()
    plt.style.use('bmh')
    return True