# Design-Project-IRAM

## Description
Files to post-process the results obtained during our Design Project on a propotype aerosol analysis instrument developped by AeroSpec. 

Working with 5 compounds: Ammonium Sulfate (AmmSul), Ammonium Oxalate (AmmOx), Glucose, Suberic Acid (SubAc), Ethyl Palmitate (EthPalm.) 

## Installation
Several dependency with python modules as Scipy and openCv.

## Usage
For each compound, a separate jupyter notebook enable to run separately the postproccessing of the data. The necessary Data and results ("OUTPUT") are given in the folder "Data".


## Authors and acknowledgment
Coded by Geza Soldati and Anna Halloy. Based on algorithm and approach proposed by Nikunj Dudani.


## Project status
This design project is finished. Could be used further alongside of the instrument.
