import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np 
import csv, json
import os
from scipy.signal import find_peaks
from scipy.interpolate import UnivariateSpline
from scipy.ndimage import gaussian_filter1d

class CPC_File:
    def __init__(self, filename):
        self.filename = filename
        self.skiprows, self.skipfooter = self._get_skiprows_skipfooter_lengths()
        self.sample_file_name = None
        self.instrument_model = None
        self.sample_numbers = None
        self.start_date = None
        self.start_time = None
        self.sample_length = None
        self.averaging_interval = None
        self.title = None
        self.instrument_id = None
        self.means = None
        self.mins = None
        self.maxs = None
        self.stds = None
        self.concentration = None
        self.elapsed_time = None
        self.elapsed_sample = None
        self._get_time_df()
        self._Extract_meta_data()
        
    def _get_skiprows_skipfooter_lengths(self):
        '''
        Find the row where the concentration data begins in the csv files and where it ends.
        Returns the number of rows to skip at header and the number of rows to skip at footer
        Return: skiprows, skipfooter
        '''
        with open(self.filename, 'r', encoding="ANSI") as csvfile:
            line = csvfile.readline()
            count = 0
            # Variable to store at what line the concentration is stored
            begin_row = None
            #iterate over the whole file
            while line:
                line_split = line.split(',')
                # Check if line corresponds to begin of elapsed time and concentration data
                if line_split[0]=='"Elapsed (s)"':
                    begin_row = count
                # Count lines
                count += 1 
                # Read next line
                line = csvfile.readline()
            return begin_row, count-(begin_row+62)
    
    def _get_time_df(self):
        '''
        Convert the DataFrame to a new format with cumulative elapsed time and combined concentration and error data
        '''
        direct_df = pd.read_csv(self.filename,
                                encoding='ANSI',
                                skiprows=self.skiprows,
                                engine="python",
                                skipfooter=self.skipfooter)
        
        new_df = pd.DataFrame(columns=['Sample', 'Elapsed Time (min)', 'Concentration'])

        for i in range(len(direct_df.columns)//2):
            elapsed = direct_df.iloc[:, 2*i]
            concentration = direct_df.iloc[:, 2*i+1]

            # Calculate the cumulative elapsed time for this minute
            elapsed_min = (i)*60

            # Get the elapsed time column correctly
            elapsed_time = elapsed+elapsed_min

            # Column with corresponding sample number
            sample_number = i+1
            sample_col = np.ones((len(elapsed)))*sample_number

            # Add the new data to the DataFrame
            temp_df = pd.DataFrame({'Sample': sample_col, 
                                    'Elapsed Time (min)': elapsed_time,
                                    'Concentration': concentration})
            new_df = pd.concat([new_df, temp_df])

        self.concentration = new_df["Concentration"].to_numpy()
        self.elapsed_time = new_df["Elapsed Time (min)"].to_numpy()
        self.elapsed_sample = new_df["Sample"].to_numpy()
        

    def _Extract_meta_data(self):
        '''
        Extracting all other data which is not the direct concentration measure of the cpc
        :return:
        '''
        with open(self.filename, 'r', encoding="ANSI") as csvfile:
            line = csvfile.readline()
            count = 0

            while line:
                line = line.replace('\n', '')
                if count==0:
                    self.sample_file_name = line.split(',')[-1]
                elif count==1:
                    self.instrument_model = line.split(',')[-1]
                elif count==3:
                    self.sample_numbers = np.array(line.split(',')[1:-1:2], dtype=np.int32)
                elif count==4:
                    self.start_date = line.split(',')[1::2]
                elif count==5:
                    self.start_time = line.split(',')[1::2]
                elif count==6:
                    self.sample_length = line.split(',')[1::2]
                elif count==7:
                    self.averaging_interval = line.split(',')[1::2]
                elif count==8:
                    self.title = line.split(',')[1:]
                elif count==9:
                    self.instrument_id = line.split(',')[1::2]
                elif count==11:
                    self.means = np.array(line.split(',')[1::2], dtype=np.float64)
                elif count==12:
                    self.mins = np.array(line.split(',')[1::2],dtype=np.float64)
                elif count==13:
                    self.maxs = np.array(line.split(',')[1::2],dtype=np.float64)
                elif count==14:
                    self.stds = np.array(line.split(',')[1::2],dtype=np.float64)

                elif count>15:
                    break

                line = csvfile.readline()
                count += 1
                
    def extract_experiment_conc(self, start_sample, end_sample):
        ''' 
        Function to extract only one sample from concentration
        '''
        sample_numbers = np.arange(start_sample, end_sample+1)
        rows = [ (i in sample_numbers) for i in self.elapsed_sample]
        return self.concentration[rows]
    
# FUNCTIONS ASSOCIATED WITH CPC ANALYSIS

def AnalyseExpConcTruncated(conc, peak_height=1500, expName = 'Noname', output_folder = '',
                            vline_height = 10000):
    '''

    :param conc: Concentration of the CPC
    :param peak_height: expected peak height improving the peak finder algorithm
    :param expName: name of the experiment
    :param output_folder: folder to save the ouput (image and csv)
    :return: mean concentration before collection, after collection, mean between them (C_base),
     their difference (C_error), collection concentration, collection durationn
    '''
    conc = np.array(conc)
    if len(conc)<10:
        print("Error, len of exp to low...")
        return False
    
    #conc_x = np.arange(len(conc))
    #conc_spl = UnivariateSpline(conc_x, conc)
    #conc_grad = np.gradient(conc_spl(conc_x))

    smoothed_data = gaussian_filter1d(list(conc), sigma=20)
    smoothed_conc_grad = np.gradient(smoothed_data)
    sm_peaks1, _ = find_peaks(-smoothed_conc_grad)
    sm_peaks2,_ = find_peaks(smoothed_conc_grad)
    if len(sm_peaks1)>1:
        #print("More than 1 peak found, taking the strongest one")
        sm_peak1 = sm_peaks1[np.argmin(smoothed_conc_grad[sm_peaks1])]
    else:
        sm_peak1 = sm_peaks1[0]
        
    if len(sm_peaks2)>1:
        #print("More than 1 peak found, taking the strongest one")
        sm_peak2 = sm_peaks2[np.argmax(smoothed_conc_grad[sm_peaks2])]
    else:
        sm_peak2 = sm_peaks2[0]
    
    conc_grad = np.gradient(conc)
    conc_grad_p1 = conc_grad[sm_peak1-60:sm_peak1+60]
    conc_grad_p2 = conc_grad[sm_peak2-60: sm_peak2+60]
    peaks1, _ = find_peaks(-conc_grad_p1, height=peak_height)
    peaks2,_ = find_peaks(conc_grad_p2, height=peak_height)
    if len(peaks1)>1:
        print("More than 1 peak found, taking the strongest one")
        peak1 = peaks1[np.argmin(conc_grad_p1[peaks1])]
    else:
        peak1 = peaks1[0]
        
    if len(peaks2)>1:
        print("More than 1 peak found, taking the strongest one")
        peak2 = peaks2[np.argmax(conc_grad_p2[peaks2])]
    else:
        peak2 = peaks2[0]
    
    peak1 = sm_peak1-60 + peak1
    peak2 = sm_peak2-60 + peak2
    
    S_begin = max(0, peak1-4*60)
    S_end = min(len(conc), peak2+4*60)
    
    X = np.arange(S_begin,S_end)
    
    C_before = conc[S_begin:peak1].mean()
    C_collection = conc[peak1:peak2+1].mean() # = conc_on in Nikunj's Code
    C_after = conc[peak2:S_end+1].mean()
    
    C_base = (C_after + C_before)/2 # = conc_ss in Nikunj's code
    C_error = abs(C_after-C_before)/2  # = for particle error
    
    Time_collection = peak2-peak1

    fig, ax = plt.subplots(layout='constrained', figsize=(6,3.5))
    ax.grid()
    ax.plot(X, conc[X], color='black', label='CPC Conc. Measure')
    #ax.plot(X, conc_grad[X], alpha=0.5, label='Conc. gradient')
    ax.hlines(C_base, xmin=S_begin, xmax=X[-1], linestyles='-', alpha = 0.7, colors='#8B5C95', label = 'Conc. w/o col.')
    ax.hlines(C_collection, xmin=S_begin, xmax=X[-1], linestyles='-', alpha=0.8, colors='#7A9BFF', label = 'Conc. during col.')
    ax.hlines([C_before, C_after],xmin=S_begin, xmax=X[-1], linestyles='--', color='#C6A8D2', label = 'Conc. error')
    ax.vlines([peak1], ymin=0, ymax=vline_height, linestyles='-', alpha=0.8, color='green', label= 'Col. start')
    ax.vlines([peak2], ymin=0, ymax=vline_height, linestyles='-', alpha=0.8, color='red', label= 'Col. stop')
    #ax.fill_between(np.arange(peak1,peak2), C_before, C_after, alpha=0.4, color = '#D2A8C7', label='Conc. error')
    ax.set_ylabel(r'Concentration [$p \cdot cm^{-3}$]')
    ax.set_xlabel('Elapsed time [s]')
    fig.legend(bbox_to_anchor=(1, -0.2), loc='lower right', ncol = 3)
    #fig.legend( bbox_to_anchor=(1.15, 0.6), loc='center')
    fig.savefig(os.path.join(output_folder,f"{expName}ConcentrationImage.jpg"), dpi=500 , bbox_inches='tight')
    plt.show()
    
    return C_before, C_after, C_base, C_error, C_collection, Time_collection

def extractAllCPCExperimentsFromExcel(folder_name=None, output_folder=None, data_path=None, excel_sample_filename="samples.xlsx"):
    '''
    Applying extraction, analysis and plotting of all CPC experiments listed in a excel with a specific format
    :param folder_name: folder containing the excel file and a "CPC_data" folder
    :param excel_sample_filename: filename of the excel (without the directroy)
    :return:
    '''
    # Get the directories
    if not os.path.exists(data_path):
        data_path = os.path.join(folder_name, "CPC_data")
    if not os.path.exists(output_folder):
        output_folder = os.path.join(folder_name, "OUTPUT")

    # Open excel with all the information on the experiments
    all_experiment_excel = pd.read_excel(os.path.join(folder_name, excel_sample_filename))

    # Iterate over all experiments
    old_filename = None
    for exp in all_experiment_excel["Sample"].unique():
        print(f"Processing experiment {exp}........")
        rows = all_experiment_excel[all_experiment_excel["Sample"] == exp]

        filename = rows["AIM_file"].unique()
        start_exp = rows["cut_lower"].unique()
        end_exp = rows["cut_upper"].unique()

        if len(filename) > 1:
            raise ValueError(f"Error: Filename not unique for experiment {exp}")
        else:
            filename = os.path.join(data_path, filename[0])
        if len(start_exp) > 1:
            raise ValueError(f"Error: Lower cut number not correct for experiment {exp}")
        else:
            start_exp = start_exp[0]
        if len(end_exp) > 1:
            raise ValueError(f"Error: Upper cut number not correct for experiment {exp}")
        else:
            end_exp = end_exp[0]

        if not filename == old_filename:
            AmmSulCPCFile = CPC_File(filename)
            old_filename = filename

        # Get the experiment's concentration
        ExpConc = AmmSulCPCFile.extract_experiment_conc(start_sample=start_exp, end_sample=end_exp)

        # Analyze the experiment's concentrations
        C_before, C_after, C_base, C_error, C_collection, C_time = AnalyseExpConcTruncated(
            ExpConc,
            peak_height=1500,
            expName=f"Exp{exp}",
            output_folder=output_folder
        )

        # Dictionary to structure the result to be saved
        results = {
            'Filename': filename,
            'Experiment Number': exp,
            'Start sample': start_exp,
            'End sample': end_exp,
            'Concentration before': C_before,
            'Concentration after': C_after,
            'Concentration base (mean)': C_base,
            'Concentration error (deviation)': C_error,
            'Concentration collection (mean)': C_collection,
            'Collection time (sec)': C_time,
            'Total collection (part per cm3)': (C_base - C_collection) * C_time,
            'Total collection error (part per cm3)': C_error * C_time
        }

        # Open a new CSV file for writing
        with open(os.path.join(output_folder, f"Exp{exp}CPCMeasuresResults.csv"), "w", newline="") as csvfile:
            # Define the field names for the CSV file
            fieldnames = list(results.keys())

            # Create a writer object for the CSV file and write the header row
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            # Write the data rows to the CSV file
            writer.writerow(results)

'''
def extracAllCPCExperimentsFromExcel(folder_name = "Data/AmmSul", excel_sample_filname = "samples.xlsx"):
    
    # Get the directories
    data_path = folder_name+"/CPC_data/"
    output_folder = folder_name+"/OUTPUT/"
    
    # Open excel with all the information on the experiments
    all_experiment_excel = pd.read_excel(folder_name+"/"+"samples.xlsx")
    
    # Iterate over all experiment
    old_filename = None
    for exp in all_experiment_excel["Sample"].unique():
        print(f"Processing experiment {exp}........")
        rows = all_experiment_excel[all_experiment_excel["Sample"]==exp]
        
        filename = rows["AIM_file"].unique()
        start_exp = rows["cut_lower"].unique()
        end_exp = rows["cut_upper"].unique()
        
        if len(filename)>1:
            print(f"error, filename not unique for exp {exp}")
        else :
            filename = data_path + filename[0]
        if len(start_exp)>1:
            print(f"error, lower cut number not correct for exp {exp}")
        else:
            start_exp = start_exp[0]
        if len(end_exp)>1:
            print(f"error, upper cut number not correct for exp {exp}")
        else:
            end_exp = end_exp[0]
        
        if not filename==old_filename:
            AmmSulCPCFile = CPC_File(filename)
            old_filename = filename
        
        # Get the experiments concentration
        ExpConc = AmmSulCPCFile.extract_experiment_conc(start_sample=start_exp, end_sample=end_exp)
        
        # Analyse the experiments concentrations
        C_before, C_after, C_base, C_error, C_collection, C_time = AnalyseExpConcTruncated(ExpConc, h=1500, expName = "Exp"+str(exp), output_folder = output_folder)
        
        # Dictionary to structure the result to be saved
        results = {'Filename': filename,
                'Experiment Number': exp,
                'Start sample': start_exp,
                'End sample': end_exp,
                'Concentration before': C_before,
                'Concentration after': C_after,
                'Concentration base (mean)': C_base,
                'Concentration error (deviation)': C_error,
                'Concentration collection (mean)': C_collection,
                'Collection time (sec)': C_time,
                'Total collection (part per cm3)': (C_base-C_collection)*C_time,
                'Total collection error (part per cm3)': C_error * C_time}

        # Open a new CSV file for writing
        with open(output_folder + f'Exp{exp}CPCMeasuresResults.csv', 'w', newline='') as csvfile:
            # Define the field names for the CSV file
            fieldnames = list(results.keys())

            # Create a writer object for the CSV file and write the header row
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            # Write the data rows to the CSV file
            writer.writerow(results)
            '''