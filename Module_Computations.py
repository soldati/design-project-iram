import os
import pandas as pd 
import numpy as np
import json

from RGB_Collection_area import GetDiameterFromImage

def Particles2Absorbances(experiments_excel_dataframe, data_path, output_folder, flow = 5.95*1000/60, fixed_collection_diameter=2.54, ComputeDiameterFromImage=True, print_values=False):
    data_dict = {
        "Collection_diameter":[],
        "Masses" : [],
        "Masses_error" : [],
        "N_as" : [],
        "N_as_error" : [],
        "Exps":[],
        "Abs_array":[],
        "Wv_array":[]
    }
    
    for exp in experiments_excel_dataframe["Sample"].unique():
        #Load the data
        CPC_result_path = os.path.join(output_folder,f"Exp{exp}CPCMeasuresResults.csv")
        FTIR_result_path =os.path.join(output_folder,f"Exp{exp}FTIRResults.json")
        # Load array from csv
        cpc_df = pd.read_csv(CPC_result_path)
        # Load dict of FTIR data from json 
        with open (FTIR_result_path, "r") as f:
            ftir_dict = json.loads(f.read())
        
        # Get the cpc values 
        #particle_count = cpc_df["Concentration collection (mean)"].values[-1]*flow
        particle_count_total = cpc_df["Total collection (part per cm3)"].values[-1]*flow # units: part/cm^3*s * cm^3/s = particules
        CPC_error_total = cpc_df["Total collection error (part per cm3)"].values[-1]*flow # units: ... = particules
        
        # Take the row with 6mm aperture of the experiment
        row = experiments_excel_dataframe[experiments_excel_dataframe["Sample"]==exp]
        row = row[row["aperture"]==6]
        # Extract the infos about the compound and particle
        dia = row["dia"].values[-1] # units: nm
        rho = row["rho"].values[-1] # units: g/cm^3
        molar_mass = row["M"] # units : g/mol

        # Calculate the mass of the total particle and error
        particle_volume = (4/3*np.pi* (dia * 10**(-7) /2)**3) # (dia is in nm -> to cm) -> cm^3
        m_count_total = particle_count_total*particle_volume*rho #grams /cm^3
        m_count_error = CPC_error_total*particle_volume*rho #grams

        # Get the collection diameter from image of the crystal
        if ComputeDiameterFromImage == True:
            RGB_img_name = row["IMAGE_File"].values[0] 
            RGB_img_filepath = os.path.join(data_path, "Pictures", RGB_img_name)
            if os.path.exists(RGB_img_filepath):
                collection_diameter = GetDiameterFromImage(RGB_img_filepath, output_folder, exp_num = exp,
                                                        beam_diameter=10.8, crystal_diameter=25.4)
                collection_diameter = collection_diameter/10 # from mm to cm
                if collection_diameter==None:
                    print("Error.. NO diameter measured ... taking whole crystal")
                    collection_diameter = 2.54
            else :
                print("! Assuming the whole crystal as collection area...")
                collection_diameter = 2.54 #whole crystal
        else :
            collection_diameter = fixed_collection_diameter # whole crystal

        # Save the result diameter in dictionary
        data_dict["Collection_diameter"].append(collection_diameter)
        
        # Compute collection area
        collection_area = np.pi*(collection_diameter/2)**2

        # Another measure: particle per cm3 of crystal
        N_a = particle_count_total*particle_volume/collection_area #particle count divided by air of the total crystal
        N_a_error = CPC_error_total*particle_volume/collection_area #particle count error divided by air of the total crystal
        
        # Printing the values for debugging or else
        if print_values == True:
            print(f"Magnitude count total: {np.log10(particle_count_total)}, and particle volume {np.log10(particle_volume)},part. diam:{dia},rho: {rho} flow: {flow}, col area:{collection_area}")
        
        # Get ftir results and save it into dictionary
        data_dict["Abs_array"].append(ftir_dict["absorbance_peaks"])
        data_dict["Wv_array"].append(ftir_dict["wavenumber_peaks"])
        
        # Append to a global list
        data_dict["Exps"].append(int(exp))
        data_dict["Masses"].append(m_count_total.tolist()) 
        data_dict["Masses_error"].append(m_count_error.tolist())
        data_dict["N_as"].append(N_a.tolist())
        data_dict["N_as_error"].append(N_a_error.tolist())
    
    # save the dictionary in json        
    output_json_filename = os.path.join(output_folder,'ParticlesAndAbsorbances.json')
    with open(output_json_filename, 'w') as json_file:
        js = json.dumps(data_dict)
        json_file.write(js)
    
    
    Masses = data_dict["Masses"]
    Masses_error = data_dict["Masses_error"]
    N_as = data_dict["N_as"]
    N_as_error = data_dict["N_as_error"]
    Exps = data_dict["Exps"]
    
    return data_dict


