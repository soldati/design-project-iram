import cv2
import numpy as np
import os

def detect_circle_contour(RGB):

    # Convert to grayscale
    gray = cv2.cvtColor(RGB, cv2.COLOR_RGB2GRAY)

    # Blur the image to reduce noise
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)

    # Apply Hough Circle Transform to detect the circle
    circles = cv2.HoughCircles(blurred, cv2.HOUGH_GRADIENT, dp=1, minDist=50, param1=100, param2=30, minRadius=0,
                               maxRadius=0)

    if circles is not None:
        # Round the circle parameters and convert to integers
        circles = np.round(circles[0, :]).astype(int)

        # Get the first detected circle
        (x, y, radius) = circles[0]

        # Draw the detected circle on the RGB image
        result_image = RGB.copy()
        cv2.circle(result_image, (x, y), radius, (0, 0, 255), 2)

        # Calculate the area of the circle contour
        circle_area = np.pi * radius ** 2

        return result_image, (x, y, radius), circle_area

    # If no circle is found, return None
    return None, None, None


def detect_patch_area(RGB, circle_contour):
    # Extract circle contour parameters
    (x, y, radius) = circle_contour

    # Crop the region of interest (ROI) around the circle
    roi = RGB[y - radius:y + radius, x - radius:x + radius]

    # Convert ROI to grayscale
    gray = cv2.cvtColor(roi, cv2.COLOR_RGB2GRAY)

    # Threshold the grayscale ROI to create a binary mask
    # _, binary = cv2.threshold(gray, 30, 255, cv2.THRESH_BINARY)
    _, binary = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    x_size = int(0.005 * binary.shape[0])
    y_size = int(0.005 * binary.shape[1])
    # Taking a matrix of size 5 as the kernel
    #kernel = np.ones((x_size, y_size), np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(x_size,y_size))
    # The first parameter is the original image,
    # kernel is the matrix with which image is
    # convolved and third parameter is the number
    # of iterations, which will determine how much
    # you want to erode/dilate a given image.
    img_dilation = cv2.dilate(binary, kernel, iterations=10)
    img_erosion = cv2.erode(img_dilation, kernel, iterations=30)
    img_dilation = cv2.dilate(img_erosion, kernel, iterations=20)

    # Find contours in the binary mask
    contours, _ = cv2.findContours(img_dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) > 0:
        # Find the largest contour within the circle contour
        largest_contour = max(contours, key=cv2.contourArea)

        # Adjust contour coordinates to match original image
        largest_contour[:, 0, 0] += x - radius
        largest_contour[:, 0, 1] += y - radius

        # Draw the contour on the RGB image
        result_image = RGB.copy()

        cv2.drawContours(result_image, [largest_contour], -1, (0, 255, 0), 2)
        cv2.circle(result_image, (int(x), int(y)), int(radius), (255, 0, 0), 2)

        # Calculate the area of the patch
        patch_area = cv2.contourArea(largest_contour)

        return result_image, patch_area

    # If no patch contour is found, return None
    return None, None


def AreaRatio2Diameter(area_ratio, beam_dia=10.8, crystal_dia=25.4):
    """From aeral ratio covered by collection to 

    Args:
        area_ratio (_type_): _description_
        beam_dia (float, optional): _description_. Defaults to 10.8.
        crystal_dia (float, optional): _description_. Defaults to 25.4.

    Returns:
        _type_: diameter in mm
    """
    total_area = np.pi * (crystal_dia / 2) ** 2  # in mm^2
    collection_area = area_ratio * total_area
    collection_diameter = 2 * np.sqrt(collection_area / np.pi)

    if collection_diameter < beam_dia:
        print("Collection diameter < beam diameter ... taking dia=beam diameter")
        return beam_dia

    return collection_diameter

def GetDiameterFromImage(image_filepath, output_folder="", exp_num=0,
                         beam_diameter=10.8, crystal_diameter=25.4):
    '''

    :param image_filepath: path to image file
    :param beam_diameter: default is 10.8 mm
    :param crystal_diameter: default is 25.4 mm
    :return:
    '''
    if not os.path.exists(image_filepath):
        print("Cant get to the image filepath")
        return None

    RGB = cv2.imread(image_filepath)

    # Detect circle contour and calculate area
    result_image, circle_contour, circle_area = detect_circle_contour(RGB)

    if result_image is not None:
        # Display the result image with the circle contour
        #cv2.imshow('Circle Contour', result_image)
        #cv2.waitKey(0)

        # Detect patch inside the circle and calculate area
        result_image, patch_area = detect_patch_area(RGB, circle_contour)

        if result_image is not None:

            # Draw the enclosing circle of the patch on the result image
            # cv2.drawContours(RGB, [result_image], 0, (0, 0, 255), 2)

            output_filepath = os.path.join(output_folder, f"Exp{exp_num}CollectionArea.jpg")
            cv2.imwrite(output_filepath, result_image)

            # Display the result image with the patch contour
            #cv2.imshow('Patch Contour', result_image)
            #cv2.waitKey(0)


            # Calculate the relative patch area ratio
            relative_area_ratio = patch_area / circle_area
            if relative_area_ratio>1:
                return None
            collection_diameter = AreaRatio2Diameter(relative_area_ratio,
                                                     beam_dia=beam_diameter,
                                                     crystal_dia=crystal_diameter)
            print(f"Relative patch area ratio:{ relative_area_ratio} and diameter {collection_diameter}")
            return collection_diameter

        else:
            print('No patch contour detected.')
            return None

    print('No circle contour detected.')
    return None
