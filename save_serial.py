import serial
from pynput import keyboard
from datetime import date

exp = input("Experiment number/name: ")

# date
today = date.today()

# Initialize serial connection
ser = serial.Serial('COM5', 9600)

# Define end command
end_command = b'end\n'

# Initialize data buffer and end flag
data = b''
end_flag = False

#Discarding the fist ... lines
discard=0

# files to save
fileName=f"{today.strftime('%d%m%Y')}FlowDataExp{exp}.txt" #name of the CSV file generated

file = open(fileName, "w")
print(f"Created file to save: {fileName}")

# Define function to handle keyboard input
def on_press(key):
    global end_flag
    try:
        # Check if key is 'q'
        if key.char == '§':
            end_flag = True
    except AttributeError:
        pass

# Initialize keyboard listener
listener = keyboard.Listener(on_press=on_press)
listener.start()

line_num = 0
print_all_X_lines = 10
# Start data retrieval loop
while not end_flag:
    while line_num < discard:
        print("Discarding initial samples")
        line_num = line_num+1
    # Read data from serial port
    data = ser.readline()
    line=data.decode('UTF-8').strip()  # remove newline character
    if line_num % print_all_X_lines == 0:
            print(line)
    # Append data to file
    file.write(line + '\n')
    line_num += 1 

# Stop keyboard listener
listener.stop()

# Close serial connection
ser.close()


print("Data collection complete!")
file.close()
