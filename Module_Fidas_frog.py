import pandas as pd
import matplotlib.pyplot as plt
import datetime
import numpy as np
import csv, json
import os
from scipy.signal import find_peaks
plt.style.use('bmh')

class FidasFrogData:
    def __init__(self, filename):
        self.filename = filename
        self.header = {}
        self.header_lines = 0
        self._read_header()
        self.units = None
        self.timestamps = None
        self.elapsed_seconds = None
        self.PM1 = None
        self.PM2_5 = None
        self.PM4 = None
        self.PM10 = None
        self.PMtot = None
        self.dCn = None
        self._extract_data()

    def _read_header(self):
        '''
        Gets the metadata from Fidas frog file
        :return:
        '''
        line_count = 0
        with open(self.filename, 'r') as file:
            current_key = None
            for line in file:
                line_count += 1
                line = line.strip()
                if line == '':
                    continue  # Stop reading after encountering an empty line

                if line.startswith('timestamp'):
                    line_count -= 1  # Exclude the line starting with "timestamp" from the count
                    self.header_lines = line_count
                    break

                if ':' in line:
                    try:
                        key, value = line.split(':', 1)
                        self.header[key] = value
                    except ValueError:
                        key = line.split(':', 1)
                        print(key)
                        self.header[key] = None
                    current_key = key
                elif current_key:
                    self.header[current_key] += ' ' + line





    def _extract_data(self):
        '''
        Extracts the information from Fidas Frog .txt file
        :return:
        '''
        frog_dataframe = pd.read_csv(filename, sep="\t", skiprows=self.header_lines, encoding="ANSI", decimal=",")
        self.units = frog_dataframe.columns.values.tolist()
        self.timestamps = frog_dataframe.iloc[:,0].to_numpy()
        self.PM1 = frog_dataframe.iloc[:,1].to_numpy()
        self.PM2_5 = frog_dataframe.iloc[:,2].to_numpy()
        self.PM4 = frog_dataframe.iloc[:,3].to_numpy()
        self.PM10 = frog_dataframe.iloc[:,4].to_numpy()
        self.PMtot = frog_dataframe.iloc[:,5].to_numpy()
        self.dCn = frog_dataframe.iloc[:,6].to_numpy()
        self.df = frog_dataframe.iloc[:,:7].copy()

        time = pd.to_datetime(frog_dataframe["timestamp"], format='%d.%m.%Y - %H:%M:%S')
        self.elapsed_seconds = (time-time[0]).dt.total_seconds().to_numpy()

        self.df.columns = ["Elapsed Time", "PM1", "PM2_5", "PM4", "PM10", "PMtot", "dCn"]
        self.df["Elapsed Time"] = (time-time[0]).dt.to_pytimedelta()

    def extract_sample(self, start_str = 0, stop_str = 0):
        '''
        Get the range related to 1 experience ("sample")
        :return:
        '''
        # Convert reference elapsed times to timedelta objects
        start_time = pd.to_timedelta(start_str)
        end_time = pd.to_timedelta(stop_str)

        # Filter the dataframe to extract the range between the start and end times
        filtered_df = self.df[
            (pd.to_timedelta(self.df['Elapsed Time']) >= start_time) &
            (pd.to_timedelta(self.df['Elapsed Time']) <= end_time)
            ]
        return filtered_df